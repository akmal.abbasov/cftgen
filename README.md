## cftgen

A tool to generate AWS CloudFormation template.

## Installation

### Requirements:

- Python: >= 3.5

### Setting up
```
$ git clone https://gitlab.com/akmal.abbasov/cftgen.git
$ cd cftgen
$ sudo apt install virtualenv
$ virtualenv env -p python3
$ source env/bin/activate
$ python setup.py install
```

### Running
```
$ python -m cftgen --config-file tests/sample_conf/config.ini
```

`cftgen` expects configuration for the template.

Currently only `Metadata` section is supported, but it's possible to add support
for other resources, such as VPC, InternetGateway, ...

Example configuration file.
```ini
[Metadata]
env=ApiDev
build=development
vpc=Dev
owner=Foo industries
service=ServiceVPC
```

