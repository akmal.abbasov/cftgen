import os
import unittest

from cftgen.config import Parser as ConfigParser
from cftgen.template import Generator as TemplateGenerator


class TestTemplateGenerator(unittest.TestCase):
    def test_generated_template(self):
        """Run cftgen with valid config and compare with expected template"""
        config = ConfigParser('{0}/sample_conf/config.ini'.format(os.path.dirname(os.path.abspath(__file__))))

        generated_template = TemplateGenerator(config).to_json()
        expected_template = open('{0}/sample_output/worksample.cfn.template'.format(os.path.dirname(os.path.abspath(__file__)))).read().rstrip()

        self.assertEqual(generated_template, expected_template)
