import os
import unittest

from cftgen.config import Parser as ConfigParser
from cftgen.config import ConfigFileNotFound
from cftgen.config import InvalidConfigFile


class TestConfigParser(unittest.TestCase):
    def test_init_not_existing_file(self):
        """Run cftgen with not existing config file."""
        with self.assertRaises(ConfigFileNotFound):
            ConfigParser('not_existing')

    def test_init_invalid_file(self):
        """Run cftgen with unsupported type of config file."""
        with self.assertRaises(InvalidConfigFile):
            ConfigParser('{0}/sample_conf/invalid_config_format.yaml'.format(os.path.dirname(os.path.abspath(__file__))))

    def test_config_metadata_section(self):
        """Run cftgen with valid config and verify metadata section parsing"""
        config = ConfigParser('{0}/sample_conf/config.ini'.format(os.path.dirname(os.path.abspath(__file__))))
        self.assertEqual(config.metadata.env, 'ApiDev')
        self.assertEqual(config.metadata.build, 'development')
        self.assertEqual(config.metadata.vpc, 'Dev')
        self.assertEqual(config.metadata.owner, 'Foo industries')
        self.assertEqual(config.metadata.service, 'ServiceVPC')
