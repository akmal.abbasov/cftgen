"""
cftgen.cli

The command-line interface in charge of processing arguments and calling the tool.
"""

import argparse
import logging

from cftgen.config import Parser as ConfigParser
from cftgen.config import ConfigFileNotFound
from cftgen.config import InvalidConfigFile

from cftgen.template import Generator as TemplateGenerator


def main(args):
    parser = argparse.ArgumentParser(prog='cftgen', description='Generate AWS CloudFormation template.')
    parser.add_argument('--config-file', required=True)
    args = vars(parser.parse_args())

    try:
        cfg = ConfigParser(args['config_file'])
        print(TemplateGenerator(cfg).to_json())
    except InvalidConfigFile:
        logging.error('Invalid configuration file.')
    except ConfigFileNotFound:
        logging.error('Configuration file not found.')
