"""
cftgen.template

The class used to generate AWS CloudFormation Template based on given configuration.
"""

from troposphere import Ref, Tags, Template, Output, Join
from troposphere.ec2 import VPC, VPCGatewayAttachment, PortRange, DHCPOptions, \
    NetworkAcl, NetworkAclEntry, InternetGateway, SecurityGroup, VPCDHCPOptionsAssociation
from troposphere.sns import Topic


class Generator:
    def __init__(self, config):
        self.template = Template()
        self.__generate_template(config)

    def to_json(self):
        return self.template.to_json()

    def __generate_template(self, config):
        self.template.add_description('Service VPC - used for services')
        self.template.add_metadata({
            "Build": config.metadata.build,
            "DependsOn": [],
            "Environment": config.metadata.env,
            "Revision": "develop",
            "StackName": "ApiDev-Dev-VPC",
            "StackType": "InfrastructureResource",
            "TemplateBucket": "cfn-apidev",
            "TemplateName": "VPC",
            "TemplatePath": "ApiDev/Dev/VPC"
        })

        shared_tags = {'Environment': config.metadata.env, 'Owner': config.metadata.owner, 'Service': config.metadata.service, 'VPC': config.metadata.vpc}

        vpc = self.template.add_resource(
            VPC(
                'VPC',
                CidrBlock='10.0.0.0/16',
                EnableDnsHostnames='true',
                EnableDnsSupport='true',
                InstanceTenancy='default',
                Tags=Tags(**{'Name': "{0}-{1}-ServiceVPC".format(config.metadata.env, config.metadata.vpc)}, **shared_tags)
            ))

        bastion_sg = self.template.add_resource(
            SecurityGroup(
                'BastionSG',
                GroupDescription='Used for source/dest rules',
                Tags=Tags(**{'Name': "{0}-{1}-VPC-Bastion-SG".format(config.metadata.env, config.metadata.vpc)}, **shared_tags),
                VpcId=Ref(vpc)
            ))

        network_acl = self.template.add_resource(
            NetworkAcl(
                'VpcNetworkAcl',
                Tags=Tags(**{'Name': "{0}-{1}-NetworkAcl".format(config.metadata.env, config.metadata.vpc)}, **shared_tags),
                VpcId=Ref(vpc)
            ))

        dhcp_option = self.template.add_resource(
            DHCPOptions(
                'DhcpOptions',
                DomainName=Join('', [Ref('AWS::Region'), '.compute.internal']),
                DomainNameServers=['AmazonProvidedDNS'],
                Tags=Tags(**{'Name': "{0}-{1}-DhcpOptions".format(config.metadata.env, config.metadata.vpc)}, **shared_tags)
            ))

        self.template.add_resource(
            VPCDHCPOptionsAssociation(
                'VpcDhcpOptionsAssociation',
                DhcpOptionsId=Ref(dhcp_option),
                VpcId=Ref(vpc)
            )
        )

        self.template.add_resource(
            NetworkAclEntry(
                'VpcNetworkAclSsh',
                NetworkAclId=Ref(network_acl),
                RuleNumber=10000,
                RuleAction='allow',
                Protocol='6',
                PortRange=PortRange(To='22', From='22'),
                Egress='false',
                CidrBlock='127.0.0.1/32',
            ))

        self.template.add_resource(
            NetworkAclEntry(
                'VpcNetworkAclInboundRulePublic80',
                NetworkAclId=Ref(network_acl),
                RuleNumber=20000,
                RuleAction='allow',
                Protocol='6',
                PortRange=PortRange(To='80', From='80'),
                Egress='false',
                CidrBlock='0.0.0.0/0',
            ))

        self.template.add_resource(
            NetworkAclEntry(
                'VpcNetworkAclInboundRulePublic443',
                NetworkAclId=Ref(network_acl),
                RuleNumber=20001,
                RuleAction='allow',
                Protocol='6',
                PortRange=PortRange(To='443', From='443'),
                Egress='false',
                CidrBlock='0.0.0.0/0',
            ))

        self.template.add_resource(
            NetworkAclEntry(
                'VpcNetworkAclOutboundRule',
                NetworkAclId=Ref(network_acl),
                RuleNumber=30000,
                RuleAction='allow',
                Protocol='-1',
                Egress='true',
                CidrBlock='0.0.0.0/0',
            ))

        internet_gateway = self.template.add_resource(
            InternetGateway(
                'InternetGateway',
                Tags=Tags(**{'Name': "{0}-{1}-InternetGateway".format(config.metadata.env, config.metadata.vpc)}, **shared_tags)
            )
        )

        cloudwatch_alarm_topic = self.template.add_resource(
            Topic(
                'CloudWatchAlarmTopic',
                TopicName='ApiDev-Dev-CloudWatchAlarms'
            )
        )

        nat_emergency_topic = self.template.add_resource(
            Topic(
                'NatEmergencyTopic',
                TopicName='ApiDev-Dev-NatEmergencyTopic'
            )
        )

        self.template.add_resource(
            VPCGatewayAttachment(
                'VpcGatewayAttachment',
                InternetGatewayId=Ref(internet_gateway),
                VpcId=Ref(vpc)
            )
        )

        self.template.add_output([
            Output(
                'BastionSG',
                Value=Ref(bastion_sg)
            ),
            Output(
                'CloudWatchAlarmTopic',
                Value=Ref(cloudwatch_alarm_topic)
            ),
            Output(
                'InternetGateway',
                Value=Ref(internet_gateway)
            ),
            Output(
                'NatEmergencyTopicARN',
                Value=Ref(nat_emergency_topic)
            ),
            Output(
                'VPCID',
                Value=Ref(vpc)
            ),
            Output(
                'VPCName',
                Value=Ref('AWS::StackName')
            ),
            Output(
                'VpcNetworkAcl',
                Value=Ref(network_acl)
            ),
        ])
