"""
cftgen.main

A __main__ method for cftgen tool to make it called as a module
"""

import sys

from cftgen.cli import main as cftgen_main


def main():
    cftgen_main(sys.argv)


if __name__ == '__main__':
    main()
