"""
cftgen.config

The helper classes to parse configuration and provide easy access.
"""

import os
import configparser


class ConfigFileNotFound(OSError):
    pass


class InvalidConfigFile(TypeError):
    pass


class _Metadata:
    def __init__(self, config):
        self.env = config['env']
        self.build = config['build']
        self.vpc = config['vpc']
        self.owner = config['owner']
        self.service = config['service']


class Parser:
    def __init__(self, config_path):
        config = configparser.ConfigParser()

        try:
            # Check if given config file exists
            if not os.path.isfile(config_path):
                raise ConfigFileNotFound

            # Read metadata configs
            config.read(config_path)
            self.metadata = _Metadata(config['Metadata'])

        except configparser.ParsingError:
            raise InvalidConfigFile
