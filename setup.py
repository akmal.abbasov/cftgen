#!/usr/bin/env python
"""
cftgen setup file.
"""

from setuptools import setup, find_packages

with open('requirements.txt') as requirements_file:
    requirements = requirements_file.readlines()

setup(
    name='cftgen',
    author='Akmal Abbasov',
    author_email='akmal.abbasov@gmail.com',
    description='A tool to generate AWS Cloudformation template',
    packages=find_packages(exclude=['tests']),
    install_requires=requirements,
    python_requires='~=3.5',
    classifiers=[
        'Development Status:: 3 - Alpha',
        'Programming Language :: Python :: 3.5',
    ],
    version='0.1.0a1'
)
